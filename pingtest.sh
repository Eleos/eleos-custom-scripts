#!/bin/bash
# Author : Eleos
# Really short script that I use everytime to verify if I can ping stuff
# correctly

count=5
timeout=5

ping -c $count -W $timeout 8.8.8.8
ping -c $count -W $timeout eleos.space
