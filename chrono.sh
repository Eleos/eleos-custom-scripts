#!/bin/bash
# Author: Eleos (but stolen from the internet)
# Simple chronometer. Cause we all need one sometimes and I don't want to open
# some webbrowser or use my phone

date1=$(date +%s); while true; do 
   echo -ne "$(date -u --date @$(($(date +%s) - date1)) +%H:%M:%S)\r";
done
