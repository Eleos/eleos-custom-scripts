#!/bin/bash
# Author : Eleos
# You need to previously create a borg repo before using this.

if [ -z "$1" ] ; then
    echo "Incorrect format, expected: backup_home.sh output"
    exit
fi

cont=N
echo -n "Do you want to do a backup of $HOME in $1 ? [y/N]:"
read -r cont
if [ "$cont" != "y" ] && [ "$cont" != "Y" ]; then
    echo "exiting"
    exit
fi

borg create \
  --compression lz4 \
  --stats \
  --progress \
  --exclude-from "$HOME/.config/backup/excl" \
  "$1"::'{hostname}-{now}' "$HOME"
