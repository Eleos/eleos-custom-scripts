# custom_scripts

Some custom scripts (mostly shell scripts) I'm using here and there to make my life easier.

Please read and be sure to understand the content of a script before using it on your server/laptop, I'm not responsable of your mistakes.
