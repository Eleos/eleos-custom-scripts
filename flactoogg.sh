#!/bin/bash
# Author : Eleos (but it's mostly stolen)
# Start this crate at the root of the album you want to convert to ogg
# Note : This do NOT erase your flac files

mkdir ogg

for a in ./*.flac; do
    < /dev/null ffmpeg -i "$a" -acodec libvorbis "ogg/${a%.flac}.ogg"
done
